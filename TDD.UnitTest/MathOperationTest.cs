﻿using System;
using NUnit.Framework;

namespace TDD.UnitTest
{
    public class MathOperationTest
    {
        #region Add

        [Test]
        [TestCase(2, 2, 4)]
        [TestCase(0, 2, 2)]
        [TestCase(-1, 2, 1)]
        [TestCase(-1, -2, -3)]
        [TestCase(1, -2, -1)]
        [TestCase(5.5, -2, 3.5)]
        [TestCase(1, 2.2, 3.2)]
        [TestCase("ala", "-ma", "ala-ma")]
        public void Add_TwoObject_CorrectCalculation<T>(T a, T b, T resultExpected)
        {
            // Arrange:
            var calc = new MathOperation();

            // Act:
            T result = calc.Add<T>(a, b);

            // Assert:
            Assert.AreEqual(resultExpected, result);
        }


        public void AddHelperMethod<T>(T a, T b, T resultExpected)
        {
            // Arrange:
            var calc = new MathOperation();

            // Act:
            T result = calc.Add<T>(a, b);

            // Assert:
            Assert.AreEqual(resultExpected, result);
        }

        [Test]
        public void Add_TwoInt_CorrectCalculation()
        {
            AddHelperMethod<int>(2, 3, 5);
            AddHelperMethod<double>(5.5, 2.1, 7.6);
            AddHelperMethod<float>(5.5F, 2.1F, 7.6F);
            AddHelperMethod<string>("ala ", " ma", "ala  ma");
        }

        [Test]
        public void Add_NullObject_ThrowException()
        {
            // Arrange:
            var calc = new MathOperation();

            // Assert:
            Assert.Throws<ArgumentOutOfRangeException>(() => calc.Add<int?>(null, null));
        }

        #endregion
    }
}
