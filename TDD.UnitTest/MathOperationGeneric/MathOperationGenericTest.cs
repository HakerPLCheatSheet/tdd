﻿using System;
using NUnit.Framework;
using TDD.MathOperationGeneric;

namespace TDD.UnitTest.MathOperationGeneric
{
    public class MathOperationGenericTest
    {
        #region SuportedType

        [Test]
        public void SuportedType_Double_ThrowsException()
        {
            Assert.Throws<InvalidOperationException>(() => new MathOperationGeneric<double>());
        }

        #endregion

        #region Add

        [Test]
        [TestCase(2, 2, 4)]
        [TestCase(0, 2, 2)]
        [TestCase(-1, 2, 1)]
        [TestCase(-1, -2, -3)]
        [TestCase(1, -2, -1)]
        public void Add_TwoInt_CorrectCalculation(int a, int b, int resultExpected)
        {
            // Arrange:
            var mathOperation = new MathOperationGeneric<int>();
            
            // Act:
            int result = mathOperation.Add(a, b);

            // Assert:
            Assert.AreEqual(resultExpected, result);
        }

        [Test]
        [TestCase(2, 2, 4)]
        [TestCase(0, 2, 2)]
        [TestCase(-1, 2, 1)]
        [TestCase(-1, -2, -3)]
        [TestCase(1, -2, -1)]
        [TestCase(1.7, 2, 3.7)]
        [TestCase(1.7, 1, 2.7)]
        [TestCase(1.7, -2, -0.3)]
        public void Add_TwoDecimal_CorrectCalculation(decimal a, decimal b, decimal resultExpected)
        {
            // Arrange:
            var mathOperation = new MathOperationGeneric<decimal>();

            // Act:
            decimal result = mathOperation.Add(a, b);

            // Assert:
            Assert.AreEqual(resultExpected, result);
        }

        #endregion
    }
}
