﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace TDD.UnitTest
{
    public class CalculatePointTest
    {
        #region Calculate

        [Test]
        [TestCase(3, 0, 1, 300)]
        [TestCase(3, 1, 1, 200)]
        [TestCase(10, 0, 1, 1000 + 500)]
        [TestCase(10, 1, 1, 900 + 500)]
        [TestCase(10, 1, 2, 1800 + 1000)]
        [TestCase(10, 10, 2, 0 + 500)]
        [TestCase(0, 0, 1, 0)]
        [TestCase(0, 4, 1, 0)]
        public void Calculate_PositiveValue_ReturnsCorrectPoints(
            int killedEnemys,
            int killedVillagers,
            int multiplicator,
            int expectedPoints)
        {
            // Arrange
            var calculatePoint = new CalculatePoint();

            // Act
            var points = calculatePoint.Calculate(killedEnemys, killedVillagers, multiplicator);

            // Assert
            Assert.That(points, Is.EqualTo(expectedPoints));
        }

        [Test]
        public void Calculate_NegativeKilledEnemies_ThrowsExceptions()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                new CalculatePoint().Calculate(-1, 0, 1));
        }

        [Test]
        public void Calculate_NegativeKilledVillagers_ThrowsExceptions()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                new CalculatePoint().Calculate(0, -1, 1));
        }

        [Test]
        public void Calculate_NegativeOrZeroMultiplicator_ThrowsExceptions([Values(-1, 0)] int multiplicator)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                new CalculatePoint().Calculate(0, 0, multiplicator));
        }

        #endregion
    }
}
