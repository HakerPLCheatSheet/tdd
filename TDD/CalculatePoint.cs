﻿using System;

namespace TDD
{
    public class CalculatePoint
    {
        private int _pointForEnemy = 100;
        private int _bonusPointEveryKiledEnemy = 10;
        private int _bonusPointKiledEnemy = 500;
        private int _losePointForVillager = 100;

        private int _resetMultiplerForKillVillager = 10;

        public object Calculate(int killedEnemys, int killedVillagers, int multiplicator)
        {
            if(killedEnemys < 0)
                throw new ArgumentOutOfRangeException("killedEnemys must by positive");

            if (killedVillagers < 0)
                throw new ArgumentOutOfRangeException("killedVillagers must by positive");

            if (multiplicator <= 0)
                throw new ArgumentOutOfRangeException("multiplicator must by greater then 0");

            var points = killedEnemys * _pointForEnemy;
            points += Convert.ToInt32(Math.Floor(Convert.ToDouble(killedEnemys) / Convert.ToDouble(_bonusPointEveryKiledEnemy)) * _bonusPointKiledEnemy);
            points -= killedVillagers * _losePointForVillager;

            if (killedVillagers >= _resetMultiplerForKillVillager)
                multiplicator = 1;

            points *= multiplicator;

            return Math.Max(points, 0);
        }
    }
}
