﻿using System;

namespace TDD
{
    public class MathOperation
    {
        public T Add<T>(T a, T b)
        {
            if (a == null)
                throw new ArgumentOutOfRangeException("a is null");

            if (b == null)
                throw new ArgumentOutOfRangeException("b is null");

            dynamic x = a;
            dynamic y = b;

            return x + y;
        }

        public T Subtract<T>(T a, T b)
        {
            if (a == null)
                throw new ArgumentOutOfRangeException("a is null");

            if (b == null)
                throw new ArgumentOutOfRangeException("b is null");

            dynamic x = a;
            dynamic y = b;

            return x - y;
        }

        public T Multiplication<T>(T a, T b)
        {
            if (a == null)
                throw new ArgumentOutOfRangeException("a is null");

            if (b == null)
                throw new ArgumentOutOfRangeException("b is null");

            dynamic x = a;
            dynamic y = b;

            return x * y;
        }

        public T Division<T>(T a, T b)
        {
            if (a == null)
                throw new ArgumentOutOfRangeException("a is null");

            if (b == null)
                throw new ArgumentOutOfRangeException("b is null");

            dynamic x = a;
            dynamic y = b;

            if(y == 0)
                throw new ArgumentOutOfRangeException("b division by zero");

            return x / y;
        }
    }
}
