﻿using System;

namespace TDD.MathOperationGeneric
{
    abstract class MathProvider<T>
    {
        public abstract T Divide(T a, T b);
        public abstract T Multiply(T a, T b);
        public abstract T Add(T a, T b);
        public abstract T Negate(T a);
        public virtual T Subtract(T a, T b)
        {
            return Add(a, Negate(b));
        }
    }

    class DoubleMathProvider : MathProvider<double>
    {
        public override double Divide(double a, double b)
        {
            return a / b;
        }

        public override double Multiply(double a, double b)
        {
            return a * b;
        }

        public override double Add(double a, double b)
        {
            return a + b;
        }

        public override double Negate(double a)
        {
            return -a;
        }
    }

    class IntMathProvider : MathProvider<int>
    {
        public override int Divide(int a, int b)
        {
            return a / b;
        }

        public override int Multiply(int a, int b)
        {
            return a * b;
        }

        public override int Add(int a, int b)
        {
            return a + b;
        }

        public override int Negate(int a)
        {
            return -a;
        }
    }

    class DecimalMathProvider : MathProvider<decimal>
    {
        public override decimal Divide(decimal a, decimal b)
        {
            return a / b;
        }

        public override decimal Multiply(decimal a, decimal b)
        {
            return a * b;
        }

        public override decimal Add(decimal a, decimal b)
        {
            return a + b;
        }

        public override decimal Negate(decimal a)
        {
            return -a;
        }
    }

    public class MathOperationGeneric<T>
    {
        private MathProvider<T> _math;

        public MathOperationGeneric()
        {
            // Here we check if we have operation class for that type.
            if (typeof(T) == typeof(int))
                _math = new IntMathProvider() as MathProvider<T>;
            else if (typeof(T) == typeof(decimal))
                _math = new DecimalMathProvider() as MathProvider<T>;

            // If we don't have we throw exception
            if (_math == null)
                throw new InvalidOperationException(
                    "Type " + typeof(T).ToString() + " is not supported by Fraction.");
        }

        public T Divide(T a, T b)
        {
            return _math.Divide(a, b);
        }

        public T Multiply(T a, T b)
        {
            return _math.Multiply(a, b);
        }

        public T Add(T a, T b)
        {
            return _math.Add(a, b);
        }

        public T Negate(T a)
        {
            return _math.Negate(a);
        }
    }
}
